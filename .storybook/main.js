module.exports = {
  stories: [
    "../stories/**/*.stories.mdx",
    "../stories/**/*.stories.@(js|jsx|ts|tsx)",
  ],
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-interactions",
    "@storybook/addon-postcss",
    "@storybook/addon-knobs",
    // {
    //   name:"@storybook/addon-postcss",
    //   options:{
    //     postcssLoaderOptions:{
    //       implementation: require('postcss')
    //     }
    //   }
    // },
    "storybook-css-modules",
  ],
  framework: "@storybook/react",
  core: {
    builder: "@storybook/builder-webpack5",
  },
};
