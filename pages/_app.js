import React,{useState,useEffect,useLayoutEffect} from 'react';
import '../styles/app.css'
import StoreContext from '../store';

export default function MyApp({Component, pageProps}){
    const [theme,setTheme] = useState(null);

    useLayoutEffect(() => {
        const theme =  localStorage.getItem("THEME") || 'light'
        setTheme(theme);
    },[])


    useEffect(() => {
        const $html = document.querySelector("html");

        $html.classList.remove('light');
        $html.classList.remove('null');
        $html.classList.remove('dark');
        $html.classList.remove('dim');
        $html.classList.add(theme);

    },[theme])

    const changeTheme = theme => {
        setTheme(theme);
        localStorage.setItem("THEME",theme);

    }
    return (
    <StoreContext.Provider value={{theme,changeTheme}}>
    <Component {...pageProps}/>
    </StoreContext.Provider>
    )
}