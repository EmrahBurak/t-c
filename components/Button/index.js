import React from "react";
import Link from "next/link";
import styles from "./button.module.css";

import cn from "classnames";

const LinkButton = ({ href, children, ...props }) => (
  <Link href={href}>
    <a {...props}>{children}</a>
  </Link>
);


const BaseButton = ({ children, ...props }) => (
  <>
    <button type="button" {...props}>
      {children}
    </button>
  </>
);

const Button = ({
  full = false,
  big = false,
  children,
  className,
  ...props
}) => {
  const Component = props.href ? LinkButton : BaseButton;
  return (
    <>
      <Component
        className={cn(
          styles.button,
          full && styles.fullWidth,
          big && styles.bigButton,
          className
        )}
        {...props}
      >
        {children}
      </Component>
    </>
  );
};

export default Button;
