import React from "react";
import cn from "classnames";

import styles from "./profile-box.module.css";

import Photo from "../Photo";
import Button from "../Button";
import { ArrowBottom } from "../icons";
import TextBody from "../Text-Body";

const ProfileBox = ({
  flat = false,
  slug = "emrahburak",
  userName = "Emrah Burak",
}) => {
  return (
    <Button className={cn([styles.box])}>
      <Photo size={39} />
      {!flat && (
        <>
          <div className={styles.body}>
            <TextBody>{userName}</TextBody>
            <TextBody className={styles.slug} bold>
              @{slug}
            </TextBody>
          </div>
          <ArrowBottom className={styles.icon} />
        </>
      )}
    </Button>
  );
};

export default ProfileBox;
