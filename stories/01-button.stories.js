import { withKnobs, boolean } from "@storybook/addon-knobs";

import Button from "../components/Button";
import NavigationButton from "../components/NavigationButton";
import Navigation from "../components/Navigation";
import { Home } from "../components/icons";
import TextTitle from "../components/Text-Title";
import ThemeButton from "../components/ThemeButton";
import Stack from "../components/stack";

export default {
  title: "Button",
  decorators: [withKnobs],
};

export const Normal = () => (
  <>
    <Button>Save</Button>
  </>
);

export const Theme = () => (
  <Stack column>
    <ThemeButton>Save</ThemeButton>
    <ThemeButton full>Save Full</ThemeButton>
    <ThemeButton full big>
      Save Big Button
    </ThemeButton>
  </Stack>
);

export const NavButton = () => (
  <>
    <NavigationButton>
      <Home />

      <TextTitle>Home</TextTitle>
    </NavigationButton>
  </>
);

export const Nav = () => {
  const flat = boolean("flat",false);
  return <Navigation flat={flat}  selectedKey="home" />;
};
